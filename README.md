# SQL Server projects

A collection of various scripts for the SQL Server.





# Processing of data from a web service

So that a web service can be called up, the * `sys.sp_OADestroy` * must be executed first.


*`SQL Server blocked access to procedure 'sys.sp_OADestroy' of component 'Ole Automation Procedures' because this component is turned off as part of the security configuration for this server. A system administrator can enable the use of 'Ole Automation Procedures' by using sp_configure. For more information about enabling 'Ole Automation Procedures', search for 'Ole Automation Procedures' in SQL Server Books Online.`*


EXEC sp_configure 'Ole Automation Procedures';
GO

sp_configure 'show advanced options', 1;
GO
RECONFIGURE;
GO
sp_configure 'Ole Automation Procedures', 1;
GO
RECONFIGURE;
GO


```sql
```







```sql
BEGIN TRANSACTION;

-- CREATE TABLE "operational_table" ----------------------------
CREATE TABLE [dbo].[operational_table] ( 
    [operational_key] BIGINT NOT NULL, 
    [EVALDATE] DATETIME2 NOT NULL, 
    [group] VARCHAR( 256 ) NOT NULL, 
    [gender] VARCHAR( 256 ) NOT NULL, 
    [age] INT NOT NULL, 
    [status] VARCHAR( 256 ) NOT NULL, 
    [political] VARCHAR( 256 ) NOT NULL, 
    [species] VARCHAR( 256 ) NOT NULL, 
    [number_of_children] INT NOT NULL, 
    [spending_behavior] FLOAT NOT NULL, 
    [score] FLOAT NOT NULL, 
    [figure] VARCHAR( 256 ) NOT NULL, 
    [asdf] FLOAT NOT NULL, 
    CONSTRAINT [unique_operational_key] UNIQUE ( [operational_key] ) )
GO;
-- -------------------------------------------------------------

COMMIT TRANSACTION;
```
