BEGIN TRANSACTION;

-- CREATE TABLE "status_logger" --------------------------------
CREATE TABLE [dbo].[status_logger] ( 
	[status_id] UNIQUEIDENTIFIER NOT NULL, 
	[update_date] DATETIME2 NULL, 
	[execution_time] REAL NULL, 
	[number_of_operational_records] BIGINT NULL, 
	[number_of_historical_records] BIGINT NULL, 
	[deleted_old_records] INT NULL, 
	[newly_entered_data_sets] VARCHAR( 256 ) NULL, 
	[additional_table_status] VARCHAR( 256 ) NULL, 
	CONSTRAINT [unique_status_id] UNIQUE ( [status_id] ) )
GO;
-- -------------------------------------------------------------

COMMIT TRANSACTION;
