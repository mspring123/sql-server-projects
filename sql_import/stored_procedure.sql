use TestDb
GO

drop procedure if exists dbo.testfile;
GO
create procedure dbo.testfile
as
begin try 
	declare @path varchar(max) = '/Users/asdf/Desktop/testfile.csv';
	declare @table varchar(max) = '[TestDb].dbo.operational_table';
	declare @sql_bulk varchar(max);
	declare @@elapse float = cast(getdate() as float)
	set @sql_bulk = 'bulk insert [TestDb].dbo.operational_table  from ''' + @path + ''' with
        (
			datafiletype = ''widechar'', fieldterminator = '','', rowterminator = ''\n'',
			firstrow = 2,
			--fire_triggers
        )'
	exec (@sql_bulk);
end try
begin catch
	if error_number() = 4860
		insert into dbo.status_logger values (getdate(), @@elapse, null, null, null, 'INVALID_PATH EXCEPTION', @path)
	else
		return @@elapse
end catch
GO

--exec dbo.testfile