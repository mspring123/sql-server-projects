BEGIN TRANSACTION;

-- CREATE TABLE "operational_table" ----------------------------
CREATE TABLE [dbo].[operational_table] ( 
	[operational_key] BIGINT NOT NULL, 
	[EVALDATE] DATETIME2 NOT NULL, 
	[group] VARCHAR( 256 ) NULL, 
	[gender] VARCHAR( 256 ) NULL, 
	[age] INT NULL, 
	[status] VARCHAR( 256 ) NULL, 
	[political] VARCHAR( 256 ) NULL, 
	[species] VARCHAR( 256 ) NULL, 
	[number_of_children] INT NULL, 
	[spending_behavior] FLOAT( 53 ) NULL, 
	[score] FLOAT( 53 ) NULL, 
	[figure] FLOAT( 53 ) NULL, 
	[asdf] VARCHAR( 256 ) NULL, 
	CONSTRAINT [unique_operational_key] UNIQUE ( [operational_key] ) )
GO;
-- -------------------------------------------------------------

COMMIT TRANSACTION;
